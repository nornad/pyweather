import requests
from retry import retry


@retry()
def request_forecast(url: str, params: list = None) -> dict:
    session = requests.session()
    resp = session.get(url, params=params)
    return resp.json()


def forecast(apikey: str, location: str, days: int = 3, aqi: bool = False, alerts: bool = False,
             lang: str = 'en') -> dict:
    """
    Get forecast from WeatherApi

    :param apikey: Your Weather API key
    :param location: City name, geo location like '48.8567,2.3508' (lat,long).
                     See more at https://www.weatherapi.com/docs/#intro-request
    :param days: number of days
    :param aqi: True if you need an air quality information.
    :param alerts: True if you need weather alerts.
    :param lang: 'en' for English (default), 'ru' for Russian.
                 See more at https://www.weatherapi.com/docs/

    :return: WeatherAPI response data
    """
    params = [('key', apikey), ('q', location), ('days', days), ('api', 'yes' if aqi else 'no'),
              ('alerts', 'yes' if alerts else 'no'), ('lang', lang)]
    data = request_forecast(f"https://api.weatherapi.com/v1/forecast.json", params)
    return data
