import os
from weatherapi import forecast


if __name__ == '__main__':
    data = forecast(apikey=os.getenv('WEATHERAPI_KEY'), location='Калининград', lang='ru')
    location = data['location']['name']
    if data['location']['country']:
        location = f"{location} ({data['location']['country']})"

    print(f"Погода в {location} на {data['location']['localtime']}"
          f" (получена в {data['current']['last_updated']})")
    wind_dirs = {'N': 'северный', 'NW': 'северо-западный', 'NE': 'северо-восточный', 'W': 'западный',
                 'E': 'восточный', 'S': 'южный', 'SW': 'юго-западный', 'SE': 'юго-восточный',
                 'NNE': 'северо-северо-восточный', 'NNW': 'северо-северо-западный',
                 'SSE': 'юго-юго-восточный', 'SSW': 'юго-юго-западный',
                 'ENE': 'восточно-северо-восточный', 'ESE': 'восточно-юго-восточный',
                 'WNW': 'западно-северо-западный', 'WSW': 'западно-юго-западный', }
    wind_dir = wind_dirs.get(data['current']['wind_dir'], data['current']['wind_dir'])
    wind_kph = data['current']['wind_kph']
    wind_mps = round(wind_kph / 3.6, 1)
    print(f"{data['current']['temp_c']}°C, Ветер {wind_dir}, {wind_kph} км/ч ({wind_mps} м/с),"
          f" {data['current']['condition']['text']}")
