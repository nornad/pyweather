import functools
import time


def retry(retries=3, sleep=5):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            err = None
            for i in range(retries):
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    err = e
                    print(e)
                    if i < retries - 1:
                        time.sleep(sleep)
            raise err
        return wrapper
    return decorator
